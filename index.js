const express = require('express');
const { getAutores, createAutor, getAutorById,
     deleteAutorById, putAutorById, verificarAutor, postLibroAutorById,
     getLibroporidAutorById, putLibroporidAutorById, deleteLibroporidAutorById,
     verificarLibro
     } = require('./funciones');

const servidor = express();
servidor.use(express.json());

//Obtener todos los autores y crear uno nuevo.
servidor.get('/autores', getAutores);
servidor.post('/autores', createAutor);

//Peticiones sobre un autor
servidor.get('/autores/:id', verificarAutor ,getAutorById);
servidor.delete('/autores/:id', verificarAutor ,deleteAutorById);
servidor.put('/autores/:id', verificarAutor ,putAutorById);

//Obtener libros
servidor.get('/autores/:id/libros', verificarAutor ,getAutorById);
servidor.post('/autores/:id/libros', verificarAutor ,postLibroAutorById);

//Datos de libros
servidor.get('/autores/:id/libros/:idLibro', verificarLibro ,getLibroporidAutorById);
servidor.put('/autores/:id/libros/:idLibro', verificarLibro ,putLibroporidAutorById);
servidor.delete('/autores/:id/libros/:idLibro', verificarLibro ,deleteLibroporidAutorById);


servidor.listen(8080, console.log('Servidor corriendo puerto 8080'));