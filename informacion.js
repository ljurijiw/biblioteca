let autores = [
    {
        id: 1,
        nombre: 'Jorge Luis',
        apellido: 'Borges',
        fechaDeNacimiento: '24/08/1899',
        libros: [
            {
                id: 1,
                titulo: 'Ficciones',
                descripcion: 'Se trata de uno de sus mas...',
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: 'El Aleph',
                descripcion: 'Otra recopilacion de cuentos...',
                anioPublicacion: 1949
            }
        ]
    },
    {
        id: 2,
        nombre: 'Edgar Allan',
        apellido: 'Poe',
        fechaDeNacimiento: '19/01/1809',
        libros: [
            {
                id: 1,
                titulo: 'El cuervo',
                descripcion: 'Se trata de uno de sus mas...',
                anioPublicacion: 1845
            },
            {
                id: 2,
                titulo: 'El corazon delator',
                descripcion: 'Otra recopilacion de cuentos...',
                anioPublicacion: 1843
            }
        ]
    }




]



module.exports = {
    datos: autores
}