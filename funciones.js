const { restart } = require("nodemon");

let autores = [
    {
        id: 1,
        nombre: 'Jorge Luis',
        apellido: 'Borges',
        fechaDeNacimiento: '24/08/1899',
        libros: [
            {
                id: 1,
                titulo: 'Ficciones',
                descripcion: 'Se trata de uno de sus mas...',
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: 'El Aleph',
                descripcion: 'Otra recopilacion de cuentos...',
                anioPublicacion: 1949
            }
        ]
    },
    {
        id: 2,
        nombre: 'Edgar Allan',
        apellido: 'Poe',
        fechaDeNacimiento: '19/01/1809',
        libros: [
            {
                id: 1,
                titulo: 'El cuervo',
                descripcion: 'Se trata de uno de sus mas...',
                anioPublicacion: 1845
            },
            {
                id: 2,
                titulo: 'El corazon delator',
                descripcion: 'Otra recopilacion de cuentos...',
                anioPublicacion: 1843
            }
        ]
    }
]

function getAutores(req, res) {
    res.send(autores);
}

function createAutor(req, res) {
    console.log(req.body);
    autores.push(req.body);
    res.send(autores);
}

function getAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                return res.json(autor);
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function deleteAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                const index = autores.indexOf(autor);
                if (index > -1) {
                    autores.splice(index, 1);
                }
                return res.json(autores);
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function putAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                const datos = req.body;
                autor.nombre = datos.nombre;
                autor.apellido = datos.apellido;
                autor.fechaDeNacimiento = datos.fechaDeNacimiento;
                return res.json(autor);
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function getLibrosAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                return res.json(autor.libros);
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function postLibroAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                const datos = req.body;
                autor.libros.push(datos);
                return res.json(autor.libros);
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function getLibroporidAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        const idLibro = Number(req.params.idLibro);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                for (const libro of autor.libros) {
                    if(idLibro === libro.id)
                    return res.json(libro);
                }
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function putLibroporidAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        const idLibro = Number(req.params.idLibro);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                for (const libro of autor.libros) {
                    if(idLibro === libro.id){
                        libro.titulo = req.body.titulo;
                        libro.descripcion = req.body.descripcion;
                        libro.anioPublicacion = req.body.anioPublicacion;

                        return res.json(libro);
                    }
                    
                }
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

function deleteLibroporidAutorById(req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idAutor = Number(req.params.id);
        const idLibro = Number(req.params.idLibro);
        for (const autor of autores) {
            if (idAutor === autor.id) {
                for (const libro of autor.libros) {
                    if(idLibro === libro.id){
                        const index = autor.libros.indexOf(libro);
                        if (index > -1) {
                            autor.libros.splice(index, 1);
                        }
                        return res.json(autor.libros);
                    }
                }
            }
        }
        res.status(404).send('No existe ese autor');
    } else {
        res.status(406).send('ERROR');
    }
}

//Middlewares
function verificarAutor(req, res, next) {
    const autorId = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === autorId){
            console.log('Middleware autor');
            return next();
        }
    }
    res.status(404);
}

function verificarLibro(req, res, next) {
    const autorId = Number(req.params.id);
    const libroId = Number(req.params.idLibro);
    for (const autor of autores) {
        if (autor.id === autorId){
            console.log('Middleware autor');
            for (const libro of autor.libros) {
                if(libro.id === libroId){
                    console.log('Middleware libro');
                    return next();
                }
            }
            
        }res.status(404);
    }
    res.status(404);
}

module.exports = {
    getAutores,
    createAutor,
    getAutorById,
    deleteAutorById,
    putAutorById,
    getLibrosAutorById,
    postLibroAutorById,
    getLibroporidAutorById,
    putLibroporidAutorById,
    deleteLibroporidAutorById,
    verificarAutor,
    verificarLibro
}